package com.example.innowoseTask.pojo;

import lombok.Data;

@Data
public class LoginRequest {
    private String username;
    private String password;
}
