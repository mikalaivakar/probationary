package com.example.innowoseTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InnowoseTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(InnowoseTaskApplication.class, args);
	}

}
