package com.example.innowoseTask.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static com.example.innowoseTask.utils.StringUtils.TOKEN_REFRESH_EXCEPTION;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class TokenRefreshException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public TokenRefreshException(String token, String message) {
        super(String.format(TOKEN_REFRESH_EXCEPTION, token, message));
    }
}