package com.example.innowoseTask.models.enumeration;

public enum ERole {
    ROLE_TOUR_OPERATOR,
    ROLE_ADMIN,
    ROLE_GUEST,
    ROLE_USER
}