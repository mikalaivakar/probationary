package com.example.innowoseTask.utils;

public final class StringUtils {
    public static final String ROLE_IS_NOT_FOUND = "Error: Role is not found.";
    public static final String USERNAME_IS_ALREADY_TAKEN = "Error: Username is already taken!";
    public static final String EMAIL_IS_ALREADY_IN_USE = "Error: Email is already in use!";
    public static final String USER_REGISTERED_SUCCESSFULLY = "User registered successfully!";

    public static final String BEARER = "bearer";
    public static final String AUTHORIZATION = "Authorization";
    public static final String OPERATION_NOT_ALLOWED = "Operation not allowed";
    public static final String USER_NOT_FOUND = "User with name : %s not found";
    public static final String TOKEN_REFRESH_EXCEPTION = "Failed for [%s]: %s";
}
