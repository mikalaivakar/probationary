package com.example.innowoseTask.repository;

import java.util.Optional;

import com.example.innowoseTask.models.enumeration.ERole;
import com.example.innowoseTask.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}